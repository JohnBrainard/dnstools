GO=go

tools: dnstools resolver

dnstools:
	$(GO) build ./cmd/dnstools

resolver:
	$(GO) build ./cmd/resolver
