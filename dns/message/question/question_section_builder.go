package question

import (
	dnsname "bitbucket.org/JohnBrainard/dnstools/dns/message/name"
)

type QuestionSectionBuilder struct {
	qname  *dnsname.Name
	qtype  QType
	qclass QClass
}

func NewQuestionSectionBuilder() *QuestionSectionBuilder {
	return &QuestionSectionBuilder{
		qtype:  QT_HostAddress,
		qclass: QC_Internet,
	}
}

func (b *QuestionSectionBuilder) Build() QuestionSection {
	return QuestionSection{
		qname:  b.qname,
		qtype:  b.qtype,
		qclass: b.qclass,
	}
}

func (b *QuestionSectionBuilder) Name(name string) *QuestionSectionBuilder {
	b.qname = dnsname.FromString(name)
	return b
}

func (b *QuestionSectionBuilder) QType(qtype QType) *QuestionSectionBuilder {
	b.qtype = qtype
	return b
}

func (b *QuestionSectionBuilder) QTypeHostAddress() *QuestionSectionBuilder {
	return b.QType(QT_HostAddress)
}

func (b *QuestionSectionBuilder) QClass(qclass QClass) *QuestionSectionBuilder {
	b.qclass = qclass
	return b
}

func (b *QuestionSectionBuilder) QClassInternet() *QuestionSectionBuilder {
	return b.QClass(QC_Internet)
}
