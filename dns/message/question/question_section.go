package question

import (
	"encoding/binary"
	"io"

	"bitbucket.org/JohnBrainard/dnstools/dns/message/name"
)

type QuestionSection struct {
	qname  *name.Name
	qtype  QType
	qclass QClass
}

func NewQuestionSection(qname *name.Name, qtype QType, qclass QClass) *QuestionSection {
	return &QuestionSection{qname: qname, qtype: qtype, qclass: qclass}
}

func (q *QuestionSection) Name() *name.Name {
	return q.qname
}

func (q *QuestionSection) Type() QType {
	return q.qtype
}

func (q *QuestionSection) Class() QClass {
	return q.qclass
}

func (q *QuestionSection) Write(w io.Writer) error {
	if _, err := w.Write(q.qname.Bytes()); err != nil {
		return err
	}

	if err := binary.Write(w, binary.BigEndian, q.qtype); err != nil {
		return err
	}

	if err := binary.Write(w, binary.BigEndian, q.qclass); err != nil {
		return err
	}

	return nil
}

func Read(bytes []byte) (question *QuestionSection, read int, err error) {
	name, i := name.ReadName(bytes)

	remaining := bytes[i:]
	qtype := binary.BigEndian.Uint16(remaining[:2])
	qclass := binary.BigEndian.Uint16(remaining[2:4])
	read = i + 4

	return &QuestionSection{
		qname:  name,
		qtype:  QType(qtype),
		qclass: QClass(qclass),
	}, read, nil
}
