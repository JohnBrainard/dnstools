package question

type QClass uint16

const (
	QC_Internet   QClass = 1
	QC_Unassigned QClass = 2
	QC_Chaos      QClass = 3
	QC_Hesiod     QClass = 4
)
