package question

import (
	"bytes"
	"encoding/hex"
	"testing"

	"github.com/google/go-cmp/cmp"

	"bitbucket.org/JohnBrainard/dnstools/dns/message/name"
)

func TestQuestionSection_Write(t *testing.T) {
	var byteWriter bytes.Buffer

	var section QuestionSection = QuestionSection{
		qname:  name.NewName([]string{"test", "com"}),
		qtype:  QT_HostAddress,
		qclass: QC_Internet,
	}

	if err := section.Write(&byteWriter); err != nil {
		t.Fatalf("unexpected error writing request section: %v", err)
	}

	var expected = []byte("\x04\x74\x65\x73\x74\x03\x63\x6f\x6d\x00\x00\x01\x00\x01")
	var result = byteWriter.Bytes()
	if bytes.Compare(expected, result) != 0 {
		t.Errorf("expecting %s, got %s", hex.EncodeToString(expected), hex.EncodeToString(result))
	}
}

func TestRead(t *testing.T) {
	t.Run("Returns valid question section", func(t *testing.T) {
		data, _ := hex.DecodeString(`0861637469766974790777696e646f777303636f6d0000010001`)
		expectedRead := 26
		expected := &QuestionSection{
			qname: name.NewName([]string{
				"activity",
				"windows",
				"com",
			}),
			qclass: QC_Internet,
			qtype:  QT_HostAddress,
		}

		section, read, _ := Read(data)

		if expectedRead != read {
			t.Errorf("expected %d read, got %d instead", expectedRead, read)
		}

		diffOpts := cmp.Options{
			cmp.Comparer(name.NameEquals),
			cmp.AllowUnexported(QuestionSection{}),
		}

		if diff := cmp.Diff(expected, section, diffOpts); len(diff) != 0 {
			t.Errorf("result not expected: diff = \n%s", diff)
		}
	})
}
