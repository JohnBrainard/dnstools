package resource

import (
	"bytes"
	"encoding/hex"
	"reflect"
	"testing"

	"bitbucket.org/JohnBrainard/dnstools/dns/message/name"
)

func TestResourceRecord_Write(t *testing.T) {
	t.Run("Answer", func(t *testing.T) {
		expected, _ := hex.DecodeString(`c00c000100010000000000045cf28c02`)

		record := Record{
			rname:      name.NewNameReference(0b1100),
			rtype:      1,
			class:      1,
			ttl:        0,
			dataLength: 4,
			data:       []byte{92, 242, 140, 2},
		}

		var buffer bytes.Buffer
		record.Write(&buffer)

		result := buffer.Bytes()

		if bytes.Compare(expected, result) != 0 {
			t.Errorf("expecting %s, got %s instead", hex.EncodeToString(expected), hex.EncodeToString(result))
		}
	})
}

func TestRead(t *testing.T) {
	t.Run("Simple Record", func(t *testing.T) {
		requestBytes, _ := hex.DecodeString(`c00c0001000100008b5200045db8d822`)

		expected := &Record{
			rname:      name.NewNameReference(0b1100),
			rtype:      1,
			class:      1,
			ttl:        35666,
			dataLength: 4,
			data: []byte{
				93,
				184,
				216,
				34,
			}}
		expectedRead := 16

		result, read, _ := Read(requestBytes)

		if !reflect.DeepEqual(expected, result) {
			t.Errorf("expecting %v, got %v instead", expected, result)
		}

		if expectedRead != read {
			t.Errorf("expecting %d read, got %d instead", expectedRead, read)
		}
	})

	t.Run("Complex Record", func(t *testing.T) {
		requestBytes, _ := hex.DecodeString(`c00c000500010000005a000c07636c69656e7473016cc015`)

		expected := &Record{
			rname:      name.NewNameReference(0b1100),
			rtype:      5,
			class:      1,
			ttl:        90,
			dataLength: 12,
			data:       []byte("\x07clients\x01l\xC0\x15"),
		}
		expectedRead := 24

		result, read, _ := Read(requestBytes)

		if bytes.Compare(expected.data, result.data) != 0 {
			t.Errorf("expecting:\n %s, got\n %s instead", hex.Dump(expected.data), hex.Dump(result.data))
		}

		if !reflect.DeepEqual(expected, result) {
			t.Errorf("expecting %v, got %v instead", expected, result)
		}

		if expectedRead != read {
			t.Errorf("expecting %d read, got %d instead", expectedRead, read)
		}
	})
}
