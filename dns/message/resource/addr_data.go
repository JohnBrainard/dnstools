package resource

import "net"

type addrData struct {
	data
}

func newAddrData(val []byte) *addrData {
	return &addrData{data{value: val}}
}

func (d *addrData) String() string {
	ip := net.IP(d.value)
	return ip.String()
}
