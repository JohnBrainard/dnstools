package resource

import (
	"encoding/binary"
	"fmt"
)

type mxData struct {
	data
}

func newMXData(val []byte) *mxData {
	return &mxData{data{value: val}}
}

func (d *mxData) String() string {
	pref := binary.BigEndian.Uint16(d.value[0:2])
	return fmt.Sprintf("%d - %s", pref, newNameData(d.value[2:]))
}
