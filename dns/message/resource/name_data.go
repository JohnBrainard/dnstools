package resource

import "strings"

type nameData struct {
	data
}

func (n *nameData) String() string {
	var str strings.Builder
	var b = n.Bytes()

	for b[0] != 0 {
		switch b[0] {
		case 0xC0:
			str.WriteString("->")
			b = []byte{0}

		default:
			size := b[0]
			str.Write(b[1 : size+1])
			b = b[size+1:]
			if b[0] != 0 {
				str.WriteString(".")
			}
		}
	}

	return str.String()
}

func newNameData(val []byte) *nameData {
	return &nameData{data{value: val}}
}
