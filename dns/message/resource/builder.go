package resource

import (
	"math"
	"time"
)

type Builder struct {
	rname    string
	rtype    uint16
	class    uint16
	ttl      int
	rdLength int
	rData    []byte
}

func (b *Builder) Name(val string) *Builder {
	b.rname = val
	return b
}

func (b *Builder) Type(val uint16) *Builder {
	b.rtype = val
	return b
}

func (b *Builder) Class(val uint16) *Builder {
	b.class = val
	return b
}

func (b *Builder) TTL(val int) *Builder {
	b.ttl = val
	return b
}

func (b *Builder) TTLDuration(val time.Duration) *Builder {
	b.ttl = int(math.Floor(val.Seconds()))
	return b
}

func (b *Builder) Data(val []byte) *Builder {
	b.rData = val
	b.rdLength = len(val)
	return b
}
