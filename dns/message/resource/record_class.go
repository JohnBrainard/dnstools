package resource

type RecordClass uint16

const (
	Reserved   RecordClass = 0
	Internet   RecordClass = 1
	Unassigned RecordClass = 2
	Chaos      RecordClass = 3
	Hesioid    RecordClass = 4
	QClassNone RecordClass = 254
	QClassAny  RecordClass = 255
)

var recordClassNames = map[RecordClass]string{
	0:   "Reserved",
	1:   "Internet",
	2:   "Unassigned",
	3:   "Chaos",
	4:   "Hesioid",
	254: "QClassNone",
	255: "QClassAny",
}

func (r RecordClass) String() string {
	name, ok := recordClassNames[r]

	switch {
	case ok:
		return name
	case r >= 65280 && r <= 65534:
		return "Private Use"
	case r == 65535:
		return "Reserved"
	}

	return "Unassigned"
}
