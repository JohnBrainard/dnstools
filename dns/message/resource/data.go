package resource

import (
	"encoding/hex"
)

type DataType uint16

type Data interface {
	String() string
	Bytes() []byte
}

type data struct {
	value []byte
}

func newData(val []byte) *data {
	return &data{value: val}
}

func (d data) Bytes() []byte {
	return d.value
}

func (d data) String() string {
	return hex.EncodeToString(d.value)
}
