package resource

// RecordType constants based on information found on the DNS_record_types Wikipedia article.
// https://en.wikipedia.org/wiki/List_of_DNS_record_types
type RecordType uint16

const (
	A          RecordType = 1
	NS         RecordType = 2
	CNAME      RecordType = 5
	SOA        RecordType = 6
	PTR        RecordType = 12
	MX         RecordType = 15
	TXT        RecordType = 16
	RP         RecordType = 17
	AFSDB      RecordType = 18
	SIG        RecordType = 24
	KEY        RecordType = 25
	AAAA       RecordType = 28
	LOC        RecordType = 29
	SRV        RecordType = 33
	NAPTR      RecordType = 35
	KX         RecordType = 36
	CERT       RecordType = 37
	DNAME      RecordType = 39
	APL        RecordType = 42
	DS         RecordType = 43
	SSHFP      RecordType = 44
	IPSECKEY   RecordType = 45
	RRSIG      RecordType = 46
	NSEC       RecordType = 47
	DNSKEY     RecordType = 48
	DHCID      RecordType = 49
	NSEC3      RecordType = 50
	NSEC3PARAM RecordType = 51
	TLSA       RecordType = 52
	SMIMEA     RecordType = 53
	HIP        RecordType = 55
	CDS        RecordType = 59
	CDNSKEY    RecordType = 60
	OPENPGPKEY RecordType = 61
	CSYNC      RecordType = 62
	TKEY       RecordType = 249
	TSIG       RecordType = 250
	URI        RecordType = 256
	CAA        RecordType = 257
	TA         RecordType = 32768
	DLV        RecordType = 32769

	// Obsolete Record Types
	MD       RecordType = 3
	MF       RecordType = 4
	MAILA    RecordType = 254
	MB       RecordType = 7
	MG       RecordType = 8
	MR       RecordType = 9
	MINFO    RecordType = 14
	MAILB    RecordType = 253
	WKS      RecordType = 11
	NB       RecordType = 32
	NBSTAT   RecordType = 33
	NULL     RecordType = 10
	A6       RecordType = 38
	NXT      RecordType = 30
	HINFO    RecordType = 13
	X25      RecordType = 19
	ISDN     RecordType = 20
	RT       RecordType = 21
	NSAP     RecordType = 22
	NSAP_PTR RecordType = 23
	PX       RecordType = 26
	EID      RecordType = 31
	NIMLOC   RecordType = 32
	ATMA     RecordType = 34
	SINK     RecordType = 40
	GPOS     RecordType = 27
	UINFO    RecordType = 100
	UID      RecordType = 101
	GID      RecordType = 102
	UNSPEC   RecordType = 103
	SPF      RecordType = 99
	NINFO    RecordType = 56
	RKEY     RecordType = 57
	TALINK   RecordType = 58
	NID      RecordType = 104
	L32      RecordType = 105
	L64      RecordType = 106
	LP       RecordType = 107
	EUI48    RecordType = 108
	EUI64    RecordType = 109
	DOA      RecordType = 259
)

var recordTypeNames = map[RecordType]string{
	1:     "A",
	2:     "NS",
	5:     "CNAME",
	6:     "SOA",
	12:    "PTR",
	15:    "MX",
	16:    "TXT",
	17:    "RP",
	18:    "AFSDB",
	24:    "SIG",
	25:    "KEY",
	28:    "AAAA",
	29:    "LOC",
	33:    "SRV",
	35:    "NAPTR",
	36:    "KX",
	37:    "CERT",
	39:    "DNAME",
	42:    "APL",
	43:    "DS",
	44:    "SSHFP",
	45:    "IPSECKEY",
	46:    "RRSIG",
	47:    "NSEC",
	48:    "DNSKEY",
	49:    "DHCID",
	50:    "NSEC3",
	51:    "NSEC3PARAM",
	52:    "TLSA",
	53:    "SMIMEA",
	55:    "HIP",
	59:    "CDS",
	60:    "CDNSKEY",
	61:    "OPENPGPKEY",
	62:    "CSYNC",
	249:   "TKEY",
	250:   "TSIG",
	256:   "URI",
	257:   "CAA",
	32768: "TA",
	32769: "DLV",

	3:   "MD",
	4:   "MF",
	254: "MAILA",
	7:   "MB",
	8:   "MG",
	9:   "MR",
	14:  "MINFO",
	253: "MAILB",
	11:  "WKS",
	10:  "NULL",
	38:  "A6",
	30:  "NXT",
	13:  "HINFO",
	19:  "X25",
	20:  "ISDN",
	21:  "RT",
	22:  "NSAP",
	23:  "NSAP_PTR",
	26:  "PX",
	31:  "EID",
	32:  "NIMLOC",
	34:  "ATMA",
	40:  "SINK",
	27:  "GPOS",
	100: "UINFO",
	101: "UID",
	102: "GID",
	103: "UNSPEC",
	99:  "SPF",
	56:  "NINFO",
	57:  "RKEY",
	58:  "TALINK",
	104: "NID",
	105: "L32",
	106: "L64",
	107: "LP",
	108: "EUI48",
	109: "EUI64",
	259: "DOA",
}

func (r RecordType) String() string {
	name, ok := recordTypeNames[r]

	switch {
	case ok:
		return name

	case 65280 <= r && r <= 65534:
		return "Private Use"
	}

	return "Unassigned"
}
