package resource

import (
	"encoding/binary"
	"io"

	"bitbucket.org/JohnBrainard/dnstools/dns/message/name"
)

type Record struct {
	rname      *name.Name
	rtype      RecordType
	class      RecordClass
	ttl        uint32
	dataLength uint16
	data       []byte
}

func NewResourceRecord(rname *name.Name, rtype RecordType, class RecordClass, ttl uint32, rdLength uint16, rData []byte) *Record {
	return &Record{rname: rname, rtype: rtype, class: class, ttl: ttl, dataLength: rdLength, data: rData}
}

func (r *Record) Name() *name.Name {
	return r.rname
}

func (r *Record) Type() RecordType {
	return r.rtype
}

func (r *Record) Class() RecordClass {
	return r.class
}

func (r *Record) TTL() uint32 {
	return r.ttl
}

func (r *Record) RDLength() uint16 {
	return r.dataLength
}

func (r *Record) Data() Data {
	switch r.rtype {
	case CNAME, DNAME, SOA:
		return newNameData(r.data)

	case A, AAAA:
		return newAddrData(r.data)

	case MX:
		return newMXData(r.data)
		
	default:
		return newData(r.data)
	}
}

func (r *Record) Write(writer io.Writer) error {
	if err := r.rname.Write(writer); err != nil {
		return err
	}

	if err := binary.Write(writer, binary.BigEndian, r.rtype); err != nil {
		return err
	}

	if err := binary.Write(writer, binary.BigEndian, r.class); err != nil {
		return err
	}

	if err := binary.Write(writer, binary.BigEndian, r.ttl); err != nil {
		return err
	}

	if err := binary.Write(writer, binary.BigEndian, r.dataLength); err != nil {
		return err
	}

	if _, err := writer.Write(r.data); err != nil {
		return err
	}

	return nil
}

func Read(bytes []byte) (record *Record, read int, err error) {
	name, offset := name.ReadName(bytes)
	remaining := bytes[offset:]

	rtype := binary.BigEndian.Uint16(remaining[0:])
	class := binary.BigEndian.Uint16(remaining[2:])
	ttl := binary.BigEndian.Uint32(remaining[4:])
	rdLength := binary.BigEndian.Uint16(remaining[8:])
	rData := remaining[10 : 10+rdLength]

	length := offset + int(rdLength) + 10

	return &Record{
		rname:      name,
		rtype:      RecordType(rtype),
		class:      RecordClass(class),
		ttl:        ttl,
		dataLength: rdLength,
		data:       rData,
	}, length, nil
}
