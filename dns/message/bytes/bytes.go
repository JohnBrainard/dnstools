package bytes

// BoolToByte converts a bool to 0 or 1.
func BoolToByte(val bool) byte {
	if val {
		return 1
	}
	return 0
}

// ByteToBool returns a bool based on the byte value. 0 for false, everything else is true.
func ByteToBool(val byte) bool {
	if val == 0 {
		return false
	}
	return true
}
