package header

type ResponseCode int16

const responseCodeField = 12

const (
	NoError        ResponseCode = 0 << responseCodeField
	FormatError    ResponseCode = 1 << responseCodeField
	ServerFailure  ResponseCode = 2 << responseCodeField
	NameError      ResponseCode = 3 << responseCodeField
	NotImplemented ResponseCode = 4 << responseCodeField
	Refused        ResponseCode = 5 << responseCodeField
)
