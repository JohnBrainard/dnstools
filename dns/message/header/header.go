package header

import (
	"encoding/binary"
	"fmt"
	"io"
	"strings"

	bytes2 "bitbucket.org/JohnBrainard/dnstools/dns/message/bytes"
)

type Header struct {
	Flags
	id      uint16
	qdCount uint16
	anCount uint16
	nsCount uint16
	arCount uint16
}

func New(id uint16, flags Flags, questionCount, answerCount, resourceCount, additionalCount int) *Header {
	return &Header{
		id:      id,
		Flags:   flags,
		qdCount: uint16(questionCount),
		anCount: uint16(answerCount),
		nsCount: uint16(resourceCount),
		arCount: uint16(additionalCount),
	}
}

func (h *Header) ID() uint16 {
	return h.id
}

func (h *Header) QDCount() int {
	return int(h.qdCount)
}

func (h *Header) ANCount() int {
	return int(h.anCount)
}

func (h *Header) NSCount() int {
	return int(h.nsCount)
}

func (h *Header) ARCount() int {
	return int(h.arCount)
}

func (h *Header) String() string {
	var buf strings.Builder

	buf.WriteString("Header:\n")
	buf.WriteString(fmt.Sprintf("  ID: %d\n", h.id))
	buf.WriteString(fmt.Sprintf("  QR: %d\n", h.qr))
	buf.WriteString(fmt.Sprintf("  OP Code: %d\n", h.opCode))
	buf.WriteString(fmt.Sprintf("  Authoritative: %t\n", h.authoritativeAnswer))
	buf.WriteString(fmt.Sprintf("  Truncated: %t\n", h.truncation))
	buf.WriteString(fmt.Sprintf("  Recursion Desired: %t\n", h.recursionDesired))
	buf.WriteString(fmt.Sprintf("  Recursion Available: %t\n", h.recursionAvailable))
	buf.WriteString(fmt.Sprintf("Question Count: %d, Answer Count: %d, Resource Count: %d, Additional Count: %d",
		h.qdCount, h.anCount, h.nsCount, h.arCount))

	return buf.String()
}

// Read reads the header fields from a slice of bytes.
func Read(bytes []byte) (*Header, error) {
	if len(bytes) != 12 {
		return nil, fmt.Errorf("expecting 12 bytes, got %d", len(bytes))
	}

	var header Header

	header.id = binary.BigEndian.Uint16(bytes[0:2])

	header.qr = QR(bytes[2] >> 7)
	header.opCode = OpCode(bytes[2]&0b0111_1000) >> 3

	header.authoritativeAnswer = bytes2.ByteToBool((bytes[2] & 0b0000_0100) >> 2)
	header.truncation = bytes2.ByteToBool((bytes[2] & 0b0000_0010) >> 1)
	header.recursionDesired = bytes2.ByteToBool(bytes[2] & 0b0000_0001)
	header.recursionAvailable = bytes2.ByteToBool(bytes[3] & 0b1000_0000 >> 7)
	header.responseCode = ResponseCode(bytes[3] & 0b1111)

	header.qdCount = binary.BigEndian.Uint16(bytes[4:6])
	header.anCount = binary.BigEndian.Uint16(bytes[6:8])
	header.nsCount = binary.BigEndian.Uint16(bytes[8:10])
	header.arCount = binary.BigEndian.Uint16(bytes[10:12])
	return &header, nil
}

func (h *Header) Write(writer io.Writer) (size int, err error) {
	buffer := make([]byte, 12)

	binary.BigEndian.PutUint16(buffer[0:2], h.id)
	buffer[2], buffer[3] = h.Flags.AsBytes()

	binary.BigEndian.PutUint16(buffer[4:6], h.qdCount)
	binary.BigEndian.PutUint16(buffer[6:8], h.anCount)
	binary.BigEndian.PutUint16(buffer[8:10], h.nsCount)
	binary.BigEndian.PutUint16(buffer[10:12], h.arCount)

	return writer.Write(buffer)
}
