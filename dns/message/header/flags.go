package header

import (
	"bitbucket.org/JohnBrainard/dnstools/dns/message/bytes"
)

type Flags struct {
	qr                  QR
	opCode              OpCode
	authoritativeAnswer bool
	truncation          bool
	recursionDesired    bool
	recursionAvailable  bool
	responseCode        ResponseCode
}

func FlagsEquals(flags Flags, other Flags) bool {
	return flags == other
}

func NewFlags(qr QR, code OpCode, isAuthoritative, isTruncation, recursionDesired, recursionAvailable bool, responseCode ResponseCode) Flags {
	return Flags{
		qr:                  qr,
		opCode:              code,
		authoritativeAnswer: isAuthoritative,
		truncation:          isTruncation,
		recursionDesired:    recursionDesired,
		recursionAvailable:  recursionAvailable,
		responseCode:        responseCode,
	}
}

func (h *Flags) QR() QR {
	return h.qr
}

func (h *Flags) OpCode() OpCode {
	return h.opCode
}

func (h *Flags) IsAuthoritative() bool {
	return h.authoritativeAnswer
}

func (h *Flags) IsTruncated() bool {
	return h.truncation
}

func (h *Flags) IsRecursionDesired() bool {
	return h.recursionDesired
}

func (h *Flags) IsRecursionAvailable() bool {
	return h.recursionAvailable
}

func (h *Flags) ResponseCode() ResponseCode {
	return h.responseCode
}

func (h *Flags) AsBytes() (byte, byte) {
	var byteA, byteB byte

	// QR | OPCODE | AA | TC | RD
	byteA = bytes.BoolToByte(h.recursionDesired) |
		bytes.BoolToByte(h.truncation)<<1 |
		bytes.BoolToByte(h.authoritativeAnswer)<<2 |
		byte(h.opCode&0b1111)<<3 |
		byte(h.qr&0b1)

	byteB = byte(h.responseCode) << 3

	return byteA, byteB
}
