package header

import (
	"math"
	"math/rand"
)

type HeaderBuilder interface {
	RandomId() HeaderBuilder
	OpCode(opCode OpCode) HeaderBuilder
	RecursionDesired(rd bool) HeaderBuilder
	RecursionAvailable(ra bool) HeaderBuilder

	Build() *Header
}

type headerBuilder struct {
	header *Header
}

func NewQueryHeader() HeaderBuilder {
	return &headerBuilder{
		header: &Header{
			Flags: Flags{
				qr:                  Query,
				authoritativeAnswer: false,
				truncation:          false,
				recursionAvailable:  false,
				responseCode:        0,
			},
		},
	}
}

func NewResponseHeader(id uint16, rcode ResponseCode) HeaderBuilder {
	return &headerBuilder{
		header: &Header{
			id: id,
			Flags: Flags{
				qr:           Response,
				responseCode: rcode,
			},
		},
	}
}

func (b *headerBuilder) RandomId() HeaderBuilder {
	b.header.id = uint16(rand.Intn(math.MaxUint16))
	return b
}

func (b *headerBuilder) OpCode(code OpCode) HeaderBuilder {
	b.header.opCode = code
	return b
}

func (b *headerBuilder) RecursionDesired(rd bool) HeaderBuilder {
	b.header.recursionDesired = rd
	return b
}

func (b *headerBuilder) RecursionAvailable(ra bool) HeaderBuilder {
	b.header.recursionAvailable = ra
	return b
}

func (b *headerBuilder) Build() *Header {
	return b.header
}
