package header

// QR A one bit field that specifies whether this message is a query (0), or a response (1).type QR uint8
type QR uint8

const (
	Query    QR = 0
	Response    = 1
)
