package header

import (
	"bytes"
	"encoding/hex"
	"reflect"
	"testing"
)

type mockByteWriter struct {
	bytes []byte
}

func (b *mockByteWriter) Write(bytes []byte) (n int, err error) {
	b.bytes = append(b.bytes, bytes...)
	return len(bytes), nil
}

func (b *mockByteWriter) Bytes() []byte {
	return b.bytes
}

func TestHeader_Write(t *testing.T) {
	flags := NewFlags(Query, StdQuery, false, false, true, false, NoError)
	header := New(0x1757, flags, 1, 0, 0, 0)

	var buffer bytes.Buffer
	size, err := header.Write(&buffer)

	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	if 12 != size {
		t.Errorf("expected to write 12 bytes, but %d were written instead", size)
	}

	expected, _ := hex.DecodeString("175701000001000000000000")

	result := buffer.Bytes()
	if bytes.Compare(expected, result) != 0 {
		t.Errorf("expected %s, but got %s instead", hex.EncodeToString(expected), hex.EncodeToString(result))
	}
}

func TestRead(t *testing.T) {
	bytes, _ := hex.DecodeString(`d2c981800001000200000000`)

	flags := NewFlags(Response, StdQuery, false, false, true, true, NoError)
	expected := New(0xd2c9, flags, 1, 2, 0, 0)

	result, err := Read(bytes)
	if err != nil {
		t.Fatalf("unexpected error encountered in test: %v", err)
	}

	if !reflect.DeepEqual(expected, result) {
		t.Errorf("expecting %v, got %v instead", expected, result)
	}
}
