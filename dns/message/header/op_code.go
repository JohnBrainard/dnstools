package header

// OpCode is A 16 bit identifier assigned by the program that generates any kind of query.  This identifier is copied
// the corresponding reply and can be used by the requester to match up replies to outstanding queries.
type OpCode int16

const opCodePos = 1

const (
	StdQuery            OpCode = 0 << opCodePos
	InverseQuery               = 1 << opCodePos
	ServerStatusRequest        = 2 << opCodePos
)
