package name

import (
	"encoding/binary"
	"strings"
)

type Registry interface {
	NameAt(offset uint16) string
}

type bytesRegistry []byte

func NewBytesRegistry(bytes []byte) Registry {
	return bytesRegistry(bytes)
}

func (r bytesRegistry) NameAt(offset uint16) string {
	var result []string

ParseLoop:
	for {
		b := r[offset]

		switch {
		case b&pointerMask == pointerMask:
			offset = binary.BigEndian.Uint16(r[offset:offset+2]) & pointerValueMask

		case b == 0:
			break ParseLoop

		default:
			length := int(r[offset])
			offset++
			name := string(r[int(offset) : int(offset)+length])
			result = append(result, name)
			offset += uint16(length)
		}
	}

	return strings.Join(result, ".")
}
