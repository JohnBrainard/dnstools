package name

import (
	"bytes"
	"encoding/hex"
	"errors"
	"reflect"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestIsPointer(t *testing.T) {
	type test struct {
		name   string
		expect bool
		value  interface{}
	}

	tests := []test{
		{
			name:   "Byte Slice - True",
			expect: true,
			value:  []byte{0xC0, 0xFF},
		},
		{
			name:   "Uint16 - True",
			expect: true,
			value:  uint16(0xFFFF),
			// segments:  0xFFFF,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := IsPointer(tt.value)
			if tt.expect != result {
				t.Errorf("expected %t, but got %t instead for %[3]v", tt.expect, result, tt.value)
			}
		})
	}
}

func TestPointerValue(t *testing.T) {
	type test struct {
		name        string
		expect      uint16
		expectError error
		value       interface{}
	}

	tests := []test{
		{
			name:   "Byte Slice",
			expect: 0b0011_1111_1111_1111,
			value:  []byte{0xFF, 0xFF},
		},
		{
			name:   "UInt16",
			expect: 0b0011_1111_1111_1111,
			value:  uint16(0xFFFF),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result, err := PointerValue(tt.value)
			if err != tt.expectError {
				if tt.expectError == nil {
					t.Errorf("unexpected error: %v", err)
				} else {
					t.Errorf("expecting error %v, got %v instead", tt.expectError, err)
				}
			} else if tt.expect != result {
				t.Errorf("expecting %v, got %v instead", tt.expect, result)
			}

		})
	}
}

func TestName_Bytes(t *testing.T) {
	expectedResult, _ := hex.DecodeString(`076578616d706c6503636f6d00`)

	name := Name{segments: []segment{
		{value: "example"},
		{value: "com"},
	}}

	result := name.Bytes()

	if bytes.Compare(expectedResult, result) != 0 {
		t.Errorf("expecting %v, got %v instead",
			hex.EncodeToString(expectedResult),
			hex.EncodeToString(result))
	}
}

func TestName_Write(t *testing.T) {
	t.Run("Without Pointer", func(t *testing.T) {
		expected, _ := hex.DecodeString(`0373736c076773746174696303636f6d00`)

		name := Name{segments: []segment{
			{value: "ssl"},
			{value: "gstatic"},
			{value: "com"},
		}}

		var buffer bytes.Buffer
		if err := name.Write(&buffer); err != nil {
			t.Fatalf("unexpected error writing bytes: %v: %v", err, errors.Unwrap(err))
		}

		result := buffer.Bytes()

		if bytes.Compare(expected, result) != 0 {
			t.Errorf("expecting %s, got %s instead", hex.EncodeToString(expected), hex.EncodeToString(result))
		}
	})

	t.Run("With Pointer", func(t *testing.T) {
		expected, _ := hex.DecodeString(`06676c6f62616c027078c011`)

		name := Name{segments: []segment{
			{value: "global"},
			{value: "px"},
			{reference: 0b0001_0001},
		}}

		var buffer bytes.Buffer
		if err := name.Write(&buffer); err != nil {
			t.Fatalf("unexpected error writing bytes: %v: %v", err, errors.Unwrap(err))
		}

		result := buffer.Bytes()

		if bytes.Compare(expected, result) != 0 {
			t.Errorf("expecting %s, got %s instead", hex.EncodeToString(expected), hex.EncodeToString(result))
		}

	})
}

func TestReadName(t *testing.T) {
	t.Run("With Name Value", func(t *testing.T) {
		expectedName := &Name{segments: []segment{
			{value: "example"},
			{value: "com"},
		}}
		expectedOffset := 13

		bytes, _ := hex.DecodeString(`076578616d706c6503636f6d00`)
		result, offset := ReadName(bytes)

		if !reflect.DeepEqual(expectedName, result) {
			t.Errorf("expecting %v, got %v instead", expectedName, result)
		}

		if expectedOffset != offset {
			t.Errorf("expecting offset of %d, got %d instead", expectedOffset, offset)
		}
	})

	t.Run("With Reference Value", func(t *testing.T) {
		expectedName := &Name{segments: []segment{
			{reference: 0b1100},
		}}
		expectedOffset := 2

		byteValue, _ := hex.DecodeString(`c00c0001000100008b5200045db8d822`)
		result, offset := ReadName(byteValue)

		opts := cmp.Options{
			cmp.Comparer(NameEquals),
		}

		if diff := cmp.Diff(expectedName, result, opts); diff != "" {
			t.Errorf("result not expected: %s", diff)
		}

		if expectedOffset != offset {
			t.Errorf("expecting offset of %d, got %d instead", expectedOffset, offset)
		}
	})
}
