package name

import (
	"bytes"
	"encoding/binary"
	"errors"
	"io"
	"strings"
)

const pointerMask = 0b1100_0000
const pointerValueMask = 0b0011_1111_1111_1111
const pointerMaskU16 = 0b1100_0000_0000_0000

func IsPointer(val interface{}) bool {
	switch v := val.(type) {
	case []byte:
		if v[0]&pointerMask == pointerMask {
			return true
		}

	case uint16:
		result := v & pointerMaskU16
		if result == pointerMaskU16 {
			return true
		}
	}

	return false
}

func PointerValue(ptr interface{}) (uint16, error) {
	switch v := ptr.(type) {
	case []byte:
		if len(v) != 2 {
			return 0, errors.New("byte slice pointer must contain 2 bytes")
		}
		return binary.BigEndian.Uint16(v) ^ pointerMaskU16, nil

	case uint16:
		return pointerMaskU16 ^ v, nil

	default:
		return 0, errors.New("supplied value is not a supported pointer type")
	}
}

type Name struct {
	segments []segment
}

func NewName(segmentValues []string) *Name {
	segments := make([]segment, len(segmentValues))
	for i, _ := range segments {
		segments[i] = segment{value: segmentValues[i]}
	}

	return &Name{
		segments: segments,
	}
}

func NewNameReference(reference uint16) *Name {
	return &Name{
		segments: []segment{{
			reference: reference,
		}},
	}
}

func FromString(name string) *Name {
	return NewName(strings.Split(name, "."))
}

func (n *Name) Segments() int {
	return len(n.segments)
}

func (n *Name) SegmentAt(i int) Segment {
	return n.segments[i]
}

// Bytes returns a slice of bytes usable as a QName in DNS requests.
func (n *Name) Bytes() []byte {
	var buffer bytes.Buffer

	n.Write(&buffer)

	return buffer.Bytes()
}

func (n *Name) String() string {
	values := make([]string, 0, len(n.segments))
	for _, value := range n.segments {
		values = append(values, value.value)
	}
	return strings.Join(values, ".")
}

func (n *Name) Write(writer io.Writer) error {
	var hasPointer bool
	for _, part := range n.segments {
		if part.reference != 0 {
			referenceValue := 0xC000 | part.reference
			referenceBytes := make([]byte, 2)
			binary.BigEndian.PutUint16(referenceBytes, referenceValue)

			if _, err := writer.Write(referenceBytes); err != nil {
				return err
			}

			hasPointer = true
		} else {
			length := uint8(len(part.value))
			if err := binary.Write(writer, binary.BigEndian, length); err != nil {
				return err
			}

			if _, err := writer.Write([]byte(part.value)); err != nil {
				return err
			}
		}
	}

	if !hasPointer {
		if _, err := writer.Write([]byte{0}); err != nil {
			return err
		}
	}

	return nil
}

// ReadName reads a sequence of bytes into a Name type according to the DNS spec.
func ReadName(bytes []byte) (name *Name, offset int) {
	const refOctet = 0b1100_0000

	var nameParts []segment
	var read = 0

	// for bytes[0] != 0 {
	for {
		if bytes[0] == refOctet {
			reference := binary.BigEndian.Uint16(bytes[0:2]) & pointerValueMask
			part := segment{reference: reference}
			nameParts = append(nameParts, part)
			bytes = bytes[2:]
			read += 2
			break
		} else if bytes[0] == 0 {
			read++
			break
		} else {
			length := int(bytes[0])
			part := segment{value: string(bytes[1 : length+1])}
			nameParts = append(nameParts, part)
			bytes = bytes[length+1:]
			read += length + 1
		}
	}

	return &Name{segments: nameParts}, read
}

func NameEquals(name Name, other Name) bool {
	if len(name.segments) != len(other.segments) {
		return false
	}

	for i, _ := range name.segments {
		if name.segments[i] != other.segments[i] {
			return false
		}
	}

	return true
}
