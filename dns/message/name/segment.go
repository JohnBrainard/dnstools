package name

type Segment interface {
	IsPointer() bool
	Pointer() uint16
	Name() string
}

type segment struct {
	value     string
	reference uint16
}

// IsPointer returns true if this segment is a pointer.
func (n segment) IsPointer() bool {
	return n.value == ""
}

// Pointer returns the byte offset of the name this references.
func (n segment) Pointer() uint16 {
	return n.reference
}

// Name returns the segment value of this name.
func (n segment) Name() string {
	return n.value
}
