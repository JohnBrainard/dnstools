package message

import (
	"bytes"
	"encoding/hex"
	"testing"

	"github.com/google/go-cmp/cmp"

	header2 "bitbucket.org/JohnBrainard/dnstools/dns/message/header"
	dnsname "bitbucket.org/JohnBrainard/dnstools/dns/message/name"
	"bitbucket.org/JohnBrainard/dnstools/dns/message/question"
	"bitbucket.org/JohnBrainard/dnstools/dns/message/resource"
)

func TestReadMessage(t *testing.T) {
	bytes, _ := hex.DecodeString(`d2c98180000100020000000008636c69656e74733406676f6f676c6503636f6d0000010001c00c000500010000005a000c07636c69656e7473016cc015c031000100010000005a0004acd905ce`)
	// bytes, _ := hex.DecodeString(`4f53818000010008000000001d6163636f756e742d7075626c69632d736572766963652d70726f643033026f6c096570696367616d657303636f6d0000010001c00c000100010000000f000422c9c911c00c000100010000000f000423a9f9e0c00c000100010000000f00043407aa0fc00c000100010000000f000412d331f4c00c000100010000000f000422c734b1c00c000100010000000f000403e607b9c00c000100010000000f000423ab2863c00c000100010000000f000422e0e839`)
	// bytes, _ := hex.DecodeString(`4d95818000010009000000001d636174616c6f672d7075626c69632d736572766963652d70726f643036026f6c096570696367616d657303636f6d0000010001c00c00050001000000bb003d22636174616c6f6776322d7376632d70726f6430362d7075622d3737323331383137390975732d656173742d3103656c6209616d617a6f6e617773c037c04c000100010000000c000434361d3dc04c000100010000000c000422e37fe6c04c000100010000000c000422e99fb8c04c000100010000000c000422c1f4b8c04c000100010000000c000412d3a5a2c04c000100010000000c000423aea3c1c04c000100010000000c000434cea4cac04c000100010000000c00043405d9aa`)

	flags := header2.NewFlags(header2.Response, 0, false, false, true, true, 0)
	expectedMessage := &Message{
		hdr: header2.New(0xD2C9, flags, 1, 2, 0, 0),
		questions: []question.QuestionSection{
			*question.NewQuestionSection(dnsname.FromString("clients4.google.com"), question.QT_HostAddress, question.QC_Internet),
		},
		answers: []resource.Record{
			*resource.NewResourceRecord(dnsname.NewNameReference(0xC), 5, 1, 0x5A, 0xC, ([]byte)("\x07clients\x01l\xC0\x15")),
			*resource.NewResourceRecord(dnsname.NewNameReference(0x31), 1, 1, 0x5A, 0x4, []byte{0xAC, 0xD9, 0x5, 0xCE}),
		},
		additional:  []resource.Record{},
		authorities: []resource.Record{},
		registry:    dnsname.NewBytesRegistry(bytes),
	}

	result, err := ReadMessage(bytes)

	if err != nil {
		t.Fatalf("unexpected error reading message: %v", err)
	}

	opts := cmp.Options{
		cmp.AllowUnexported(Message{}, header2.Header{}, question.QuestionSection{}, resource.Record{}),
		cmp.Comparer(header2.FlagsEquals),
		cmp.Comparer(dnsname.NameEquals),
	}

	if diff := cmp.Diff(expectedMessage, result, opts); diff != "" {
		t.Errorf("result not expected: %s", diff)
	}
}

func TestMessage_WriteMessage(t *testing.T) {
	flags := header2.NewFlags(header2.Response, 0, false, false, true, true, 0)
	message := &Message{
		hdr: header2.New(0xD2C9, flags, 1, 2, 0, 0),
		questions: []question.QuestionSection{
			*question.NewQuestionSection(dnsname.FromString("clients4.google.com"), question.QT_HostAddress, question.QC_Internet),
		},
		answers: []resource.Record{
			*resource.NewResourceRecord(dnsname.NewNameReference(0xC), 5, 1, 0x5A, 0xC, ([]byte)("\x07clients\x01l\xC0\x15")),
			*resource.NewResourceRecord(dnsname.NewNameReference(0x31), 1, 1, 0x5A, 0x4, []byte{0xAC, 0xD9, 0x5, 0xCE}),
		},
		additional:  []resource.Record{},
		authorities: []resource.Record{},
	}

	var buffer bytes.Buffer
	message.WriteMessage(&buffer)

	expected, _ := hex.DecodeString(`d2c90100000100020000000008636c69656e74733406676f6f676c6503636f6d0000010001c00c000500010000005a000c07636c69656e7473016cc015c031000100010000005a0004acd905ce`)
	result := buffer.Bytes()

	if bytes.Compare(expected, result) != 0 {
		t.Errorf("expecting %s, got %s instead", hex.EncodeToString(expected), hex.EncodeToString(result))
	}
}
