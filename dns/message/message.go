package message

import (
	"bytes"
	"io"
	"strings"

	header2 "bitbucket.org/JohnBrainard/dnstools/dns/message/header"
	"bitbucket.org/JohnBrainard/dnstools/dns/message/name"
	"bitbucket.org/JohnBrainard/dnstools/dns/message/question"
	"bitbucket.org/JohnBrainard/dnstools/dns/message/resource"
)

// Message represents the message format defined in section 4.1 of RFC1035
// https://tools.ietf.org/html/rfc1035#section-4
type Message struct {
	hdr         *header2.Header
	questions   []question.QuestionSection
	answers     []resource.Record
	authorities []resource.Record
	additional  []resource.Record
	registry    name.Registry
}

// NewMessage creates a new DNS Message.
func NewMessage(hdr *header2.Header, questions []question.QuestionSection, answers []resource.Record, authorities []resource.Record, additional []resource.Record) *Message {
	return &Message{
		hdr:         hdr,
		questions:   questions,
		answers:     answers,
		authorities: authorities,
		additional:  additional,
	}
}

func (m *Message) Header() *header2.Header {
	return m.hdr
}

func (m *Message) Questions() []question.QuestionSection {
	return m.questions
}

func (m *Message) Answers() []resource.Record {
	return m.answers
}

func (m *Message) Authorities() []resource.Record {
	return m.authorities
}

func (m *Message) Additional() []resource.Record {
	return m.additional
}

func (m *Message) ExpandName(n name.Name) string {
	parts := make([]string, 0)

	for i := 0; i < n.Segments(); i++ {
		segment := n.SegmentAt(i)

		if segment.IsPointer() {
			parts = append(parts, m.registry.NameAt(segment.Pointer()))
		} else {
			parts = append(parts, segment.Name())
		}
	}

	return strings.Join(parts, ".")
}

func ReadMessage(bytes []byte) (*Message, error) {
	registry := name.NewBytesRegistry(bytes)

	header, err := header2.Read(bytes[0:12])
	if err != nil {
		return nil, err
	}

	remaining := bytes[12:]

	questions, remaining, err := readQuestionSection(remaining, header.QDCount())
	answers, remaining, err := readMessageResources(remaining, header.ANCount())
	authority, remaining, err := readMessageResources(remaining, header.ARCount())
	additional, remaining, err := readMessageResources(remaining, header.NSCount())

	return &Message{
		hdr:         header,
		questions:   questions,
		answers:     answers,
		authorities: authority,
		additional:  additional,
		registry:    registry,
	}, nil
}

func (m *Message) WriteMessage(writer io.Writer) error {
	if _, err := m.Header().Write(writer); err != nil {
		return err
	}

	for _, q := range m.questions {
		if err := q.Write(writer); err != nil {
			return err
		}
	}

	resources := append(m.answers, m.authorities...)
	resources = append(resources, m.additional...)

	for _, r := range resources {
		if err := r.Write(writer); err != nil {
			return err
		}
	}

	return nil
}

func (m *Message) Bytes() []byte {
	var buffer bytes.Buffer
	_ = m.WriteMessage(&buffer)
	return buffer.Bytes()
}

func readQuestionSection(bytes []byte, count int) (questions []question.QuestionSection, remaining []byte, err error) {
	questions = make([]question.QuestionSection, count)
	for i, _ := range questions {
		value, read, err := question.Read(bytes)
		if err != nil {
			return nil, nil, err
		}
		questions[i] = *value
		bytes = bytes[read:]
	}
	return questions, bytes, nil
}

func readMessageResources(bytes []byte, count int) (records []resource.Record, remaining []byte, err error) {
	resources := make([]resource.Record, count)
	for i, _ := range resources {
		value, read, err := resource.Read(bytes)
		if err != nil {
			return nil, nil, err
		}
		resources[i] = *value
		bytes = bytes[read:]
	}
	return resources, bytes, nil
}
