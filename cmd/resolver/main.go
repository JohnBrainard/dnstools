package main

import (
	"flag"
	"os"
)

const usage = `%s <dns-name>`

func main() {
	var name, resolver string
	var help bool

	flag.StringVar(&name, "name", "", "<host-name>")
	flag.StringVar(&resolver, "resolver", "", "<dns-server>")
	flag.BoolVar(&help, "help", false, "print usage")

	flag.Parse()

	if flag.NArg() != 0 && len(name) == 0 {
		name = flag.Arg(0)
	}

	if help {
		flag.Usage()
		os.Exit(1)
	}

	cmd := New(resolver, name)
	cmd.Execute()
}
