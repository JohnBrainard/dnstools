package main

import (
	"fmt"
	"log"
	"math/rand"
	"net"
	"os"
	"time"

	"bitbucket.org/JohnBrainard/dnstools/cmd/resolver/printer"
	"bitbucket.org/JohnBrainard/dnstools/dns/message"
	"bitbucket.org/JohnBrainard/dnstools/dns/message/header"
	"bitbucket.org/JohnBrainard/dnstools/dns/message/name"
	"bitbucket.org/JohnBrainard/dnstools/dns/message/question"
)

type Command struct {
	resolver string
	hostName string
}

func New(resolver, hostName string) *Command {
	return &Command{
		resolver: resolver,
		hostName: hostName,
	}
}

func (c *Command) Execute() {
	rand.Seed(time.Now().Unix())

	flags := header.NewFlags(header.Query, header.StdQuery, false, false, true, false, 0)
	hdr := header.New(0, flags, 1, 0, 0, 0)
	q := question.NewQuestionSection(name.FromString(c.hostName), question.QT_HostAddress, question.QC_Internet)
	// q := question.NewQuestionSection(name.FromString(c.hostName), 15, question.QC_Internet)
	msg := message.NewMessage(hdr, []question.QuestionSection{*q}, nil, nil, nil)

	udpAddr, err := net.ResolveUDPAddr("udp", fmt.Sprintf("%s:53", c.resolver))
	if err != nil {
		log.Fatal(err)
	}

	udpConn, err := net.DialUDP("udp", nil, udpAddr)
	if err != nil {
		log.Fatal(err)
	}
	defer udpConn.Close()

	udpConn.Write(msg.Bytes())

	var buffer = make([]byte, 1024)
	read, _, err := udpConn.ReadFrom(buffer)
	if err != nil {
		log.Fatal(err)
	}

	// fmt.Printf("Read: %d, Addr: %d\n", read, addr)
	// fmt.Printf("Read: %s\n", hex.EncodeToString(buffer[:read]))

	response, err := message.ReadMessage(buffer[:read])
	if err != nil {
		log.Fatal(err)
	}
	// fmt.Println(response)

	printer.PrintMessage(os.Stdout, response)
}
