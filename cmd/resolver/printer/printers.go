package printer

import (
	"io"

	"bitbucket.org/JohnBrainard/dnstools/dns/message"
)

func PrintMessage(w io.Writer, msg *message.Message) {
	p := NewMessagePrinter(msg)
	p.Print(w)
}
