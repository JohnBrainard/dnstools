package printer

import (
	"io"
	"log"
	"text/template"

	"bitbucket.org/JohnBrainard/dnstools/dns/message"
	"bitbucket.org/JohnBrainard/dnstools/dns/message/name"
)

const messageTemplate = `Message:
    Header:
        ID: {{.Message.Header.ID}}
        QR: {{.Message.Header.QR}}
        OpCode: {{.Message.Header.OpCode}}
        Is Authoritative: {{.Message.Header.IsAuthoritative}}
        Is Truncated: {{.Message.Header.IsTruncated}}
        Recursion Desired: {{.Message.Header.IsRecursionDesired}}
        Recursion Available: {{.Message.Header.IsRecursionAvailable}}
        Response Code: {{.Message.Header.ResponseCode}}

    Questions: {{.Message.Header.QDCount}}
        {{range $index, $question := .Message.Questions}}Name: {{$question.Name}}
        Type: {{$question.Type}}
        Class: {{$question.Class}}
	{{- end}}

    {{if (gt .Message.Header.ANCount 0)}}Answers: {{.Message.Header.ANCount}}
        {{- range $index, $answer := .Message.Answers}}
        Name: {{ expandName $answer.Name}}
        Type: {{$answer.Type}}
        Class: {{$answer.Class}}
        TTL: {{$answer.TTL}}
        Data: {{$answer.Data}}
        {{end}}
    {{end}}

    {{if (gt .Message.Header.ARCount 0)}}Authoritative: {{.Message.Header.ARCount}}
        {{- range $index, $answer := .Message.Authorities}}
        Name: {{$answer.Name}}
        Type: {{$answer.Type}}
        Class: {{$answer.Class}}
        TTL: {{$answer.TTL}}
        Data: {{$answer.RDLength}} - {{$answer.Data}}
        {{end}}
    {{end -}}

    {{- if (gt .Message.Header.NSCount 0)}}Additional: {{.Message.Header.NSCount}}
        {{range $index, $answer := .Message.Additional}}Name: {{$answer.Name}}
        Type: {{$answer.Type}}
        Class: {{$answer.Class}}
        TTL: {{$answer.TTL}}
        Data: {{$answer.RDLength}} - {{$answer.Data}}
        {{end}}
    {{end -}}
`

type MessagePrinter struct {
	Message *message.Message
}

func NewMessagePrinter(message *message.Message) MessagePrinter {
	return MessagePrinter{Message: message}
}

func (p *MessagePrinter) Print(writer io.Writer) {
	tpl := template.New("message template")
	tpl.Funcs(template.FuncMap{
		"expandName": func(n name.Name) string {
			return p.Message.ExpandName(n)
		},
	})
	tpl = template.Must(tpl.Parse(messageTemplate))

	// tpl := template.Must(template.New("message template").Parse(messageTemplate))

	if err := tpl.Execute(writer, p); err != nil {
		log.Fatal("error creating message", err)
	}
}
